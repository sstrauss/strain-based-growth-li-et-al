#include "FemMembranes.hpp"

namespace mdx
{

  bool FemMembranes::initialize(QWidget *parent)
  {
    mesh = currentMesh();
    if(!mesh)
      throw(QString("FemMembranes::initialize No current mesh"));

    ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw(QString("FemMembranes::initialize No cell complex"));

    cs = &mesh->ccStructure(ccName);

    // Get the solver process
    if(!getProcess(parm("Solver Process"), solverProcess))
      throw(QString("FemMembranes::initialize Unable to make solver process: %1").arg(parm("Solver Process")));
    solverProcess->initialize(parent);

    // Get the growth process
    if(!getProcess(parm("Growth Process"), growthProcess))
      throw(QString("FemMembranes::initialize Unable to make growth process: %1").arg(parm("Growth Process")));
    growthProcess->initialize(parent);

    // Get the subdivide process
    if(!getProcess(parm("Subdivide Process"), subdivideProcess))
      throw(QString("FemMembranes::initialize Unable to make subdivide process: %1").arg(parm("Subdivide Process")));
    subdivideProcess->initialize(parent);

    maxGrowthTime = parm("Max Growth Time").toDouble();

    return true;
  }

  bool FemMembranes::step()
  {
    if(!solverProcess)
      throw(QString("FemMembranes::run Solver process pointer invalid"));
    if(!growthProcess)
      throw(QString("FemMembranes::run Growth process pointer invalid"));
    if(!subdivideProcess)
      throw(QString("FemMembranes::run Subdivide process pointer invalid"));

    // If converged subdivide then grow
    if(!solverProcess->step()) {
      // Take a snapshot if required
      QString snapshotFile = parm("Snapshot File");
      if(!snapshotFile.isEmpty())
        takeSnapshot(QString("%1-%2.jpg").arg(snapshotFile).arg(snapshotCount++, 5, 10, QChar('0')));

      if(growthTime >= maxGrowthTime)
        return false;

      growthProcess->run();
      double growthDt = growthProcess->parm("Growth Dt").toDouble();
      growthTime += growthDt;
      if(stringToBool(solverProcess->parm("Print Stats")))
        mdxInfo << QString("Growth Step Time %1, step %2").arg(growthTime).arg(growthDt) << endl;

      // If we subdivide, we need to re-initialize the solver
      if(subdivideProcess->run()) {
        mesh->updateAll(ccName);
        // Reinitialize solver with new graph
        solverProcess->initSolver(cs);
      }
    }
    mesh->updatePositions(ccName);

    return true;
  }

  bool FemMembranes::rewind(QWidget *parent)
  {
    // To rewind, we'll reload the mesh
    Mesh *mesh = currentMesh();
    if(!mesh or mesh->file().isEmpty())
      throw(QString("No current mesh, cannot rewind"));
    MeshLoad meshLoad(*this);
    meshLoad.setParm("File Name", mesh->file());
    growthTime = 0;
    snapshotCount = 0;
    return meshLoad.run();
  }

  bool FemMembranes::finalize(QWidget *parent)
  {
    if(!solverProcess)
      throw(QString("FemMembranes::run Solver process pointer invalid"));

    bool result = solverProcess->finalize(parent);

    // Cleanup
    mesh = 0;
    solverProcess = 0;
    growthProcess = 0;
    subdivideProcess = 0;

    return result;
  }

  bool FemMembraneBisect::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw(QString("FemMembraneSubdivide::run No current mesh"));
    QString ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw(QString("FemMembraneSubdivide::run Invalid cell complex"));

    fem::ElasticTriangle3Attr &elementAttr = mesh->attributes().attrMap<CCIndex, fem::ElasticTriangle3>(parm("Element Attribute"));
    fem::TransIsoMaterialAttr &materialAttr = mesh->attributes().attrMap<CCIndex, fem::TransIsoMaterial>(parm("Material Attribute"));
    fem::PressureAttr &pressureAttr = mesh->attributes().attrMap<CCIndex, fem::Pressure>(parm("Pressure Attribute"));
    fem::GrowthAttr &growthAttr = mesh->attributes().attrMap<CCIndex, fem::Growth>(parm("Growth Attribute"));

    FemMembraneSubdivide sDiv(*mesh, elementAttr, materialAttr, pressureAttr, growthAttr);
    mesh->updateAll(ccName);

    return run(*mesh, mesh->ccStructure(ccName), parm("Max Area").toDouble(), &sDiv);
  }

  bool FemMembraneDivide::initialize(QWidget *parent)
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw(QString("CellDivide::step No current mesh"));

    // Call base initialize
    if(!CellTissueCell2dDivide::initialize(parent))
      return false;

    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDivide::initialize Cannot make tissue process"));
    tissueProcess->initialize(parent);

    return true;
  }

  // Run a step of cell division
  bool FemMembraneDivide::step() 
  { 
    // Pass our subdivide
    return CellTissueCell2dDivide::step(currentMesh(), subdivider()); 
  }

  REGISTER_PROCESS(FemMembranes);
  REGISTER_PROCESS(FemMembraneSolver);
  REGISTER_PROCESS(FemMembraneRefCfg);
  REGISTER_PROCESS(FemMembraneStressStrain);
  REGISTER_PROCESS(FemMembraneDerivs);
  REGISTER_PROCESS(FemMembraneMaterial);
  REGISTER_PROCESS(FemMembraneCellFacesMaterial);
  REGISTER_PROCESS(FemMembraneAnisoDir);
  REGISTER_PROCESS(FemMembranePressure);
  REGISTER_PROCESS(FemMembranePressureDerivs);
  REGISTER_PROCESS(FemMembraneSetDirichlet);
  REGISTER_PROCESS(FemMembraneDirichletDerivs);
  REGISTER_PROCESS(FemMembraneSetGrowth);
  REGISTER_PROCESS(FemMembraneCellFacesGrowth);
  REGISTER_PROCESS(FemMembraneGrowth);
  REGISTER_PROCESS(FemMembraneBisect);

  REGISTER_PROCESS(FemMembraneVisMaterial);
  REGISTER_PROCESS(FemMembraneVisGrowth);
  REGISTER_PROCESS(FemMembraneRender);

  REGISTER_PROCESS(FemMembraneTissue);
  REGISTER_PROCESS(FemMembraneDivide);

  REGISTER_PROCESS(LabelFacesFromVolumes);
}
