#ifndef FEM_MEMBRANES_HPP
#define FEM_MEMBRANES_HPP

#include <MDXProcessFem.hpp>
#include <MeshProcessSystem.hpp>
#include <MeshProcessStructure.hpp>
#include <MDXProcessTissue.hpp>
#include <MDXProcessCellDivide.hpp>
#include <Attributes.hpp>
#include <MeshProcessSystemRender.hpp>
#include <Solver.hpp>

#include <MDXProcessTissue.hpp>
#include <MDXProcessCellDivide.hpp>   
namespace mdx
{
  class FemMembraneSolver;
  class FemMembraneGrowth;
  class FemMembraneBisect;

  // Main model class
  class FemMembranes : public Process
  {
  public:
    FemMembranes(const Process &proc) : Process(proc) 
    {
      setName("Model/CCF/01 FEM Membranes");
      setDesc("FEM simulation with growth and subdivision");

      addParm("Solver Process", "Name of solver process", "Model/CCF/02 Solver");
      addParm("Growth Process", "Name of growth process", "Model/CCF/13 Growth");
      addParm("Subdivide Process", "Name of subdivision process", "Model/CCF/14 Subdivide");
      addParm("Max Growth Time", "When to stop", "250.0");
      addParm("Snapshot File", "Snapshot file, empty for no snapshots", "Snapshot/Frame");
    }

    bool initialize(QWidget *parent);
    bool step();
    bool rewind(QWidget *parent);
    bool finalize(QWidget *parent);

  private:
    Mesh *mesh = 0;
    QString ccName;
    CCStructure *cs = 0;

    FemMembraneSolver *solverProcess = 0;
    FemMembraneGrowth *growthProcess = 0;
    FemMembraneBisect *subdivideProcess = 0;

    double growthTime = 0;
    double maxGrowthTime = 0;
    int snapshotCount = 0;
  };

  class FemMembraneSolver : public fem::FemSolver
  {
  public:
    FemMembraneSolver(const Process &proc) : fem::FemSolver(proc) 
    {
      setName("Model/CCF/02 FEM Solver");
      setDesc("FEM Simulation using triangular membrane elements");

      // Update parameters with our own defaults
			setParmDefault("Stress-Strain", "Model/CCF/05 Stress-Strain");

      // Add derivatives processes
			addParm("Element Derivs", "Process for element derivatives", "Model/CCF/03 Triangle Derivs");
			addParm("Pressure Derivs", "Process for pressure derivatives", "Model/CCF/10 Pressure Derivs");
      addParm("Dirichlet Derivs", "Process for Dirichlet derivatives", "Model/CCF/10 Dirichlet Derivs");
    }
  };

  class FemMembraneDerivs : public fem::ElementDerivs
  {
  public:
    FemMembraneDerivs(const Process &proc) : ElementDerivs(proc) 
    {
      setName("Model/CCF/03 Triangle Derivs");

			setParmDefault("Element Type", "Linear Triangle");
			setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembraneRefCfg : public fem::SetRefCfg
  {
  public:
    FemMembraneRefCfg(const Process &proc) : SetRefCfg(proc) 
    {
      setName("Model/CCF/04 Reference Configuration");

			addParm("Thickness", "Thickness of the membrane element", "1.0");
			setParmDefault("Element Type", "Linear Triangle");
			setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembraneStressStrain : public fem::StressStrain
  {
  public:
    FemMembraneStressStrain(const Process &proc) : StressStrain(proc) 
    {
      setName("Model/CCF/05 Stress-Strain");

			setParmDefault("Element Type", "Linear Triangle");
			setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembraneMaterial : public fem::SetTransIsoMaterial
  {
  public:
    FemMembraneMaterial(const Process &proc) : SetTransIsoMaterial(proc) 
    {
      setName("Model/CCF/06 Material Properties");
    }
  };

  class FemMembraneCellFacesMaterial : public fem::CellsToFacesTransIsoMaterial
  {
  public:
    FemMembraneCellFacesMaterial(const Process &proc) : CellsToFacesTransIsoMaterial(proc) 
    {
      setName("Model/CCF/07 Cells to Faces Material");
    }
  };

  class FemMembraneAnisoDir : public fem::SetAnisoDir
  {
  public:
    FemMembraneAnisoDir(const Process &proc) : SetAnisoDir(proc) 
    {
      setName("Model/CCF/08 Set Aniso Dir");

      setParmDefault("Element Type", "Linear Triangle");
      setParmDefault("Element Attribute", "Triangle Element");
    }
  };

  class FemMembranePressure : public fem::SetPressure
  {
  public:
    FemMembranePressure(const Process &proc) : SetPressure(proc) 
    {
      setName("Model/CCF/09 Set Pressure");
    }
  };

  class FemMembranePressureDerivs : public fem::PressureDerivs
  {
  public:
    FemMembranePressureDerivs(const Process &proc) : PressureDerivs(proc) 
    {
      setName("Model/CCF/10 Pressure Derivs");
    }
  };

  class FemMembraneSetDirichlet : public fem::SetDirichlet
  {
  public:
    FemMembraneSetDirichlet(const Process &proc) : SetDirichlet(proc) 
    {
      setName("Model/CCF/09 Set Dirichlet");
    }
  };

  class FemMembraneDirichletDerivs : public fem::DirichletDerivs
  {
  public:
    FemMembraneDirichletDerivs(const Process &proc) : DirichletDerivs(proc) 
    {
      setName("Model/CCF/10 Dirichlet Derivs");
    }
  }; 

  class FemMembraneSetGrowth : public fem::SetGrowth
  {
  public:
    FemMembraneSetGrowth(const Process &proc) : SetGrowth(proc) 
    {
      setName("Model/CCF/11 Set Growth");
    }
  };

  class FemMembraneCellFacesGrowth : public fem::CellsToFacesGrowth
  {
  public:
    FemMembraneCellFacesGrowth(const Process &proc) : CellsToFacesGrowth(proc) 
    {
      setName("Model/CCF/12 Cells to Faces Growth");
    }
  };

  class FemMembraneGrowth : public fem::Grow
  {
  public:
    FemMembraneGrowth(const Process &proc) : Grow(proc) 
    {
      setName("Model/CCF/13 Growth");
    }
  };

  /// Subdivide object
  class FemMembraneSubdivide : public mdx::Subdivide
  {
  public:
    FemMembraneSubdivide() {}

    FemMembraneSubdivide(Mesh &mesh, fem::ElasticTriangle3Attr &elementAttr, 
        fem::TransIsoMaterialAttr &materialAttr, fem::PressureAttr &pressureAttr, fem::GrowthAttr &growthAttr) : 
      mdxSubdivide(mesh), elementSubdivide(mesh.indexAttr(), elementAttr), 
        materialSubdivide(materialAttr), pressureSubdivide(pressureAttr), growthSubdivide(growthAttr) {}

    // Method to split the element data
    void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct &ss, 
        CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(), double interpPos = 0.5) 
    {
      mdxSubdivide.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);

      // Propagate the material parameters
      if(dim == 2 or dim == 3) {
        elementSubdivide.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
        materialSubdivide.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
        pressureSubdivide.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
        growthSubdivide.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
      }
    }
    MDXSubdivide mdxSubdivide;
    fem::ElasticTriangle3::Subdivide elementSubdivide;
    fem::TransIsoMaterial::Subdivide materialSubdivide;
    fem::Pressure::Subdivide pressureSubdivide;
    fem::Growth::Subdivide growthSubdivide;
  };

  class FemMembraneBisect : public SubdivideBisectTriangle
  {
  public:
    FemMembraneBisect(const Process &proc) : SubdivideBisectTriangle(proc) 
    {
      setName("Model/CCF/14 Subdivide");

      addParm("Element Attribute", "Attribute to store nodal values", "Triangle Element");
      addParm("Material Attribute", "Name of the attribute that holds material properties", "TransIso Material");
      addParm("Pressure Attribute", "Name of the attribute that holds pressure", "Fem Pressure");
      addParm("Growth Attribute", "Name of the attribute that holds pressure", "Fem Growth");
    }
    using SubdivideBisectTriangle::run;
    bool run();
  };

  class FemMembraneVisMaterial : public fem::VisTransIsoMaterial
  {
  public:
    FemMembraneVisMaterial(const Process &proc) : VisTransIsoMaterial(proc) 
    {
      setName("Model/CCF/20 Visualize Material");
    }
  };

  class FemMembraneVisGrowth : public fem::VisGrowth
  {
  public:
    FemMembraneVisGrowth(const Process &proc) : VisGrowth(proc) 
    {
      setName("Model/CCF/21 Visualize Growth");
    }
  };


  class FemMembraneRender : public fem::VectorRender
  {
  public:
    FemMembraneRender(const Process &proc) : VectorRender(proc) 
    {
      setName("Model/CCF/22 Vector Render");
    }
  };

  class FemMembraneTissue : public CellTissueProcess
  {
  public:
    FemMembraneTissue(const Process &process) : CellTissueProcess(process) 
    {
      setName("Model/CCF/30 Cell Tissue");
    }
  };

  class FemMembraneDivide : public CellTissueCell2dDivide
  {
  public:
    FemMembraneDivide(const Process &process) : CellTissueCell2dDivide(process)
    {
      setName("Model/CCF/31 Divide Cells");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/CCF/30 Cell Tissue");
    }

    // Initialize to grab subdivider
    bool initialize(QWidget *parent);

    // Run a step of cell division
    bool step();

  private:

    FemMembraneTissue *tissueProcess = 0;
  };

  class LabelFacesFromVolumes : public Process
  {
  public:
    LabelFacesFromVolumes(const Process &process) : Process(process)
    {
      setName("Model/CCF/40 Label Faces From Volumes");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh)
        throw QString("%1::run No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No current cell complex").arg(name());

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      for(CCIndex f : cs.faces()) {
        auto cb = cs.cobounds(f);
        auto &fIdx = indexAttr[f];
        if(cb.size() == 1)
          fIdx.label = indexAttr[*cb.begin()].label;
        else
          fIdx.label = 0;
      }
      mesh->updateProperties();
      return true;
    }
  };

}
#endif

