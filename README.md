# Strain based growth Li et al

This repository contains the strain-based growth model developed for the paper Li et al. 2023 "Cell cycle-driven growth reprogramming encodes plant age into leaf morphogenesis"

The repository is organized as follows:

The folder "MDX" contains a Linux installation package of the version of the computational modeling platform MorphoDynamX which was used for this model. MDX needs to be installed to be able to run the model.

The folder "model" contains the model. It can be started either through using the VLab browser (see MDX documentation for details) or via a Linux console by typing "make run".

The folders "results" and "templates" contain the model mesh templates as well as the final model meshes and the exported CSV files from MDX.
